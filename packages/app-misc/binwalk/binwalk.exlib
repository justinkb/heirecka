# Distributed under the terms of the GNU General Public License v2
# Copyright 2014 Nicolas Braud-Santoni <nicolas+exherbo@braud-santoni.eu>

require github [ user=ReFirmLabs  tag=v${PV} ]
require setup-py [ blacklist="2" import=setuptools test=nose ]

SUMMARY="A firmware analysis tool"
DESCRIPTION="
Binwalk is a fast, easy to use tool for analyzing and extracting firmware images.
"
HOMEPAGE+=" https://www.refirmlabs.com/binwalk-open-source/"
LICENCES="MIT"

DEPENDENCIES="
    run:
      sys-apps/file
    suggestion:
        app-arch/cabextract [[
            description = [ Support for extracting Microsoft cabinet files ]
        ]]
        app-arch/cpio [[
            description = [ Support for extracting CPIO archives ]
        ]]
        app-arch/p7zip [[
            description = [ Support for extracting .7z archives ]
        ]]
        app-arch/unrar [[
            description = [ Support for extracting rar archives ]
        ]]
        dev-python/pycryptodome[python_abis:*(-)?] [[
            description = [ Support for decrypting encrypted firmware images ]
        ]]
        sys-fs/squashfs-tools [[
            description = [ Support for Squashfs filesyAstems ]
        ]]
"
# NOTE: There are certainly more possible suggestions, but for now I only
# added the ones we currently have.

# Somehow doesn't seem to respect PYTHONPATH, so on first install it fails to
# find itself and additional runs test against the installed version.
RESTRICT="test"

test_one_multibuild() {
    HOME= PYTHONPATH=$(ls -d ${PWD}/build/lib*) edo ${PYTHON} -B setup.py test
}

