# Copyright 2018 Ivan Diorditsa <ivan.diorditsa@gmail.com>
# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    require github [ user=mozilla ] cmake
else
    require cmake github [ user=mozilla ]
fi
require python [ blacklist=2 multibuild=false ]

SUMMARY="A lightweight tool for recording, replaying and debugging execution of applications"

HOMEPAGE="https://rr-project.org/ ${HOMEPAGE}"

LICENCES="BSD-2 X11"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-util/capnproto
    test:
        dev-python/pexpect[python_abis:*(-)?]
        sys-devel/gdb
"
# NOTE: bundles brotli and blake2
# https://github.com/mozilla/rr/issues/2435

# gdb doesn't work well under sydbox, e.g.:
# "ptrace: Operation not permitted."
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -Ddisable32bit:BOOL=TRUE
    -DPYTHON_EXECUTABLE:FILEPATH=${PYTHON}
)
CMAKE_SRC_CONFIGURE_TESTS=( '-DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE' )

